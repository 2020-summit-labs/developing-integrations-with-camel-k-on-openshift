# Lab 0 - Login to Your OpenShift Environment

## Get your user number

Please get your user number from the instructor.  Your username will be 'user<#>' with the password of 'openshift' from this point forward. 

## Login to OpenShift

Navigate to the bit.ly provided by the lab staff. 

![openshift-home-login.png](./images/lab0/openshift-home-login.png)

Enter your username and password. You should land on a page that looks like the following. Click on the project with your user name. 

![openshift-landing.png](./images/lab0/openshift-landing.png)

Continue to [Lab 1](/lab1.md)