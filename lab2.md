# Lab 2 - Design Your Own API with Apicurio

## Open Apicurio Studio

Head to your OpenShift console and from the Workloads tab and select 'apicurito-service' under the other resources section. 

![apicurio-service.png](./images/lab2/apicurio-service.png)

On the right hand side scroll down to the route and click it to open Apicurio. You should see a landing page like this. 

![apicurio-landing.png](./images/lab2/apicurio-landing.png)

## Create a New API

In the middle of the page there is a button for 'New API', hit the down arrow next to it and select 'New (OpenAPI 2)'. 

![apicurio-new-api.png](./images/lab2/apicurio-new-api.png)

You will be taken to a page like this. 

![apicurio-new-api-landing.png](./images/lab2/apicurio-new-api-landing.png)

On the left hand side select 'Add data type' under Data Types. 

![apicurio-add-data-type.png](./images/lab2/apicurio-add-data-type.png)

Name the data type 'Employee'.  Give it a description of 'An employee with a name, email, city, and age.'. Enter the following for the json example. 

```json 
{
	"firstName": "Mary",
	"lastName": "Cochran",
	"email": "mcochran@redhat.com",
	"city": "Charlotte, NC",
	"age": 27
}
```

At the bottom select 'No resource'. Select 'Save' 

![apicurio-data-type.png](./images/lab2/apicurio-data-type.png)

![apicurio-data-type-2.png](./images/lab2/apicurio-data-type-2.png)

## Create GET Operation

On the left hand side we can now create our API path. Select 'Add a path'. 

![apicurio-paths.png](./images/lab2/apicurio-paths.png)

Enter the path as '/employee' and click 'Add'

![apicurio-add-path.png](./images/lab2/apicurio-add-path.png)

Next, select Get to create the Get request and click 'Add Operation'.

![apicurio-path-landing.png](./images/lab2/apicurio-path-landing.png)

Enter the following in each field. Make sure to select the blue check mark each time or else the information will not save. 

- Summary: Get an Employee
- Operation ID: getEmployee
- Description: An employee with a name, email, city, and age.

Your info section should look like. Note that we leave the produces and consumes sections as 'application/json'.

![apicurio-finished-get.png](./images/lab2/apicurio-finished-get.png)

Next, we need to create a response. Under Responses select 'Add a Response'. Select 200 and then 'Add'. 

![apicurio-response-200.png](./images/lab2/apicurio-response-200.png)

We also need to ensure that our Get returns an Employee object. Under response give it a description of 'Return an Employee' and under ResponseType select 'Employee'.

![apicurio-response-employee.png](./images/lab2/apicurio-response-employee.png)

## Create POST Operation 

Scroll back up to where it lists the operations.  Select Post and then 'Add Operation'. 

![apicurio-post.png](./images/lab2/apicurio-post.png)

Enter the following in each field. Make sure to select the blue check mark each time or else the information will not save. Leave the produces and consumes as the default application/json. 

- Summary: Create an Employee
- Operation ID: createEmployee
- Description: Create an employee with a name, email, city, and age.

![apicurio-post-info.png](./images/lab2/apicurio-post-info.png)

Under Request Body, add a description of 'An Employee Object' and select the Request Body Type as 'Employee'. Next, add a response of 201 (Created) with a description of 'Employee Created'. 

![apicurio-post-completed.png](./images/lab2/apicurio-post-completed.png)

## Save your API Swagger Documentation 

Near the top of the page on the right hand side select 'Save As...' and select 'Save as json'. 

![apicurio-save-as.png](./images/lab2/apicurio-save-as.png)

This will save your swagger documentation as a download to your computer. We will use this in the next lab. 

Congratulations you have completed Lab 2! You can now move to [Lab 3](/lab3.md)
