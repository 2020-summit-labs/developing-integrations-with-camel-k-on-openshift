# Lab 1 - Login to CodeReady Workspaces and Deploy a Sample

## Login to CodeReady Workspaces

From your user project space, select the Workloads tab, then select 'codeready'.

![openshift-project-details.png](./images/lab1/openshift-project-details.png)

On the right hand side, scroll down till you see the routes. Click on the route.  

![openshift-crw-details.png](./images/lab1/openshift-crw-details.png)

You should see a page like the following. Go ahead and login with your same username and password. 

![crw-login.png](./images/lab1/crw-login.png)

After you login you will see a landing page. This shows your workspaces. We have already created a workspace for you with the name of your user. If your workspace has a gray dot next to it instead of a green one please press the triangle run button to the right to start it up. 

![crw-landing.png](./images/lab1/crw-landing.png)

From the left hand side bar double click on your workspace named user#. This will open your workspace.  We have already imported a sample project to the workspace. 

![crw-workspace-landing.png](./images/lab1/crw-workspace-landing.png)

## Evaluate the Camel Route

In the top left hand corner there is a button that looks like 2 pieces of paper, click it. Then expand the camelk-openshift folder and then the camelk folder.  Double click on the RestWithUndertow.java file to open it. 

![crw-rest-example.png](./images/lab1/crw-rest-example.png)

Take note of that there is no project structure, only the single java file. The rest configuration section selects undertow as the camel component to use. Then an endpoint is set up at /hello.  

## Deploy the Sample to OpenShift

Next, from the top bar select Terminal and then 'Open Terminal in specific container'

![crw-open-terminal.png](./images/lab1/crw-open-terminal.png)

Select 'kamel-tools'.  This will open a terminal with the tooling to deploy your CamelK integrations to OpenShift. 

![crw-terminal-kamel-tools.png](./images/lab1/crw-terminal-kamel-tools.png)

Return to your OpenShift management console to get your terminal login. In the upper right hand corner select your username and select 'Copy Login Command'

![openshift-copy-login.png](./images/lab1/openshift-copy-login.png)

This will open a new browser tab. You may need to login in again, use your same credentials.  Once logged in a page will display that says 'Display Token'. Click on 'Display Token'. 

![openshift-display-token.png](./images/lab1/openshift-display-token.png)

There will be a section labels 'Log in with this token' and underneath a command starting with 'oc login'.  Copy that whole command. 

![openshift-token.png](./images/lab1/openshift-token.png)

Now you have your command to login to openshift. Return to your terminal in CodeReady Workspaces. Type the following commands. 

```
$ cd camelk-openshift/camelk
$ <your oc login command> 
$ kamel run --name=rest-with-undertow --dependency=camel-rest --dependency=camel-undertow RestWithUndertow.java
```

You should see output like the following. 

![crw-terminal-oc-login-and-deploy.png](./images/lab1/crw-terminal-oc-login-and-deploy.png)

## Test out the API

Return Back to your OpenShift console. If you are not currently on the workloads tab return back to it. 

![oc-list-workloads.png](./images/lab1/oc-list-workloads.png)

After about 30 seconds or so you should see a 'rest-with-undertow' workload. Click on it. 

![oc-rest-with-undertow.png](./images/lab1/oc-rest-with-undertow.png)

On the right hand side bar scroll down to the routes. Click on it. Then append '/hello' to the end of the url since that is where our API is located. You should get a response like the following. 

![camel-hit-url.png](./images/lab1/camel-hit-url.png)

## Make the API Your Own

Head back to Code Ready Workspaces and update your camel code to say something else. Like Hello Yourname. 

![camel-update-code.png](./images/lab1/camel-update-code.png)

Rerun the following command in your terminal 

```
kamel run --name=rest-with-undertow —-dependency=camel-rest —-dependency=camel-undertow RestWithUndertow.java
```

This will update the code running in OpenShift. If you return to the OpenShift console quick enough you may see the workload saying rollout in progress like below.  Wait until this completes. It should only take a few seconds. 

![oc-rollout.png](./images/lab1/oc-rollout.png)

One this completed head back to your api url and refresh the screen. You should see your code updates. 

![camel-refresh-url.png](./images/lab1/camel-refresh-url.png)

Congratulations you have completed Lab 1! You can now move to [Lab 2](/lab2.md)

