# Developing Integrations with Camel K on OpenShift

## [Lab 0 - Login to Your OpenShift Environment](/lab0.md)

## [Lab 1 - Login to CodeReady Workspaces and Deploy a Sample](/lab1.md)

## [Lab 2 - Design Your Own API with Apicurio](/lab2.md)

## [Lab 3 - Import Your API to CodeReady and Deploy it to OpenShift](/lab3.md)

## [Extra Credit](/extra-credit.md)
